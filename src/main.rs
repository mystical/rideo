use std::env;
use std::process;
use device_query::DeviceQuery;
use device_query::DeviceState;
use device_query::Keycode;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 0 {
        eprintln!("needs arguments, try with {:#?} [uri]?", args[0].to_string());
        return;
    }
    if env::consts::OS == "windows" {
        process::Command::new("cls")
        .status()
        .unwrap();
    } else {
        process::Command::new("clear")
        .status()
        .unwrap();
    }

    gst::init();
    let mut player = gst::PlayBin::new("rideo").unwrap(); 
    player.set_uri(&args[1].to_string());
    player.set_volume(9.1);
    player.play();
    println!("Control the player using P, M and END keys from keyboard.");
    let device_state = DeviceState::new();
    loop {
        for key in device_state.get_keys().iter() {
            match key {
                &Keycode::End => {
                    std::process::exit(1);
                }

                &Keycode::P => {
                    if !player.is_playing() {
                        player.play();
                    } else {
                        player.pause();
                    }
                }

                &Keycode::M => {
                    player.mute();
                }
                _ => {}
            }
        }
    }
}
